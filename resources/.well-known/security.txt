Contact: mailto:tb@t0ast.cc
Encryption: https://t0ast.cc/pubkey.asc
Preferred-Languages: en,de
Canonical: https://t0ast.cc/.well-known/security.txt
