#!/bin/bash

function li {
	echo -n "<li><a href=\"$1\""
	if ! echo $1 | grep -E "^\/" > /dev/null; then  # Link is pointing to an external page
		echo -n " class=\"external\""
		# Open the page in a new tab
		echo -n " target=\"_blank\""
		# Just some security measures to prevent the external page from accessing our page
		echo -n " rel=\"noopener noreferrer\""
	fi
	echo -n ">$2</a></li>"
}

echo "
<nav>
	<ul class=\"unstyled\">"
		li "/" /
		li "/about" about
		li "/blog" blog
		if [ $IS_DEBUG_BUILD ]; then
			li "/test" test
		fi
		li "https://twitter.com/T0astBread" twitter
		li "https://gitlab.com/T0astBread" gitlab
		li "https://github.com/T0astBread" github
		li "https://news.ycombinator.com/user?id=t0astbread" hn
echo "
	</ul>
</nav>
<div class=\"nav-spacer\"></div>"
