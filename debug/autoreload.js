/**
 * How this works:
 * 
 * 1. Fetch initial primer:
 *    This primer will be the reference "ID" to check if the build
 *    has changed
 * 2. Continue to re-fetch and compare the primer every 500ms and
 *    reload when a change has been detected
 */

(async () => {
    const fetchPrimer = async () =>
        fetch("/primer", {
            cache: "reload"
        }).then(res => res.text())

    const primer = await fetchPrimer()

    const checkPrimer = async () => {
        const newPrimer = await fetchPrimer()
        if(newPrimer !== primer) {
            setTimeout(() => location.reload(), 500)
        }
        else {
            setTimeout(checkPrimer, 500)
        }
    }

    await checkPrimer()
})()
