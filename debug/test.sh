#!/bin/bash
components/stylesheets.sh main
components/title.sh

components/nav.sh
echo "<main>"
echo "<h1>Page content</h1>"
echo "
Also see [test2](/test2.html)

Enim aut ipsum quia sequi et illo placeat. Eum nulla quaerat quis animi. Vero quas aut nam qui facilis rerum aut. Corrupti veniam repudiandae ad fugiat dolor.

Sunt inventore et ut iusto ut. Repellendus et est est vero et facilis natus eos. Et laboriosam autem enim quis eum.

Optio deserunt sit debitis voluptates ut aspernatur ut. Tenetur eum illo laudantium. Ut nostrum qui ut temporibus. Sit sunt saepe perferendis laboriosam est maiores necessitatibus officiis. Odit laborum illo doloremque non praesentium rerum qui.

Nostrum in odit et. Ducimus eligendi architecto iste. Recusandae voluptates unde a et enim. Reprehenderit quasi voluptatem consequatur consequuntur ut. Dignissimos soluta qui placeat et aut doloribus omnis asperiores. Eos quod ipsum sequi vel error itaque vero qui.

\`\`\`javascript
const $ = document.querySelector
\$(\".hello-world\")
\`\`\`

\`\`\`html
$(cat auxiliary/web-app-in-a-tweet.txt | sed -r 's/.{70}/\0\n/g')
\`\`\`

Voluptas est cum aliquam numquam nisi et. Dolorem nisi laboriosam laboriosam iusto eos blanditiis dolorem. Quos sed eius in saepe quod magni quam. Voluptas enim asperiores ratione magnam ipsam. Recusandae sint cumque asperiores.

<pre class=\"sourceCode\">
$(cat auxiliary/fish_prompt.fish | sed -r 's/.{60}/\0\n/g')
</pre>
" | pandoc - -s

echo "<img src=\"https://avatars2.githubusercontent.com/u/24869055?s=400&v=4\"/>"
echo "<small>uwu</small>"
echo "<strong>strong</strong>"
echo "</main>"
