function fish_prompt
	if set -q VIRTUAL_ENV
		set_color yellow
		echo -n "py("
		# set_color --bold yellow
		echo -n (basename "$VIRTUAL_ENV")
		# set_color normal
		# set_color yellow
		echo -n ")"
		set_color normal
		echo -n " "
	end

	function _is_git_clean
		return (git status -s | wc -l)
	end

	function _git_branch_name
		set -l branch (git symbolic-ref --quiet HEAD ^/dev/null)
		if set -q branch[1]
			echo (string replace -r '^refs/heads/' '' $branch)
		else
			echo (git rev-parse --short HEAD ^/dev/null)
		end
	end

	function _print_git_branch_status
		set -l a (_git_branch_name)
		set -l b origin/(_git_branch_name)

		set -l base ( git merge-base $a $b )
		set -l aref ( git rev-parse  $a    )
		set -l bref ( git rev-parse  $b    )

		if [ $aref = $bref ]
		#	echo up-to-date
		else if [ $aref = $base ]
			echo -n "|🔻"  # Remote is ahead
		else if [ $bref = $base ]
			echo -n "|🔺"  # Remote is behind
		else
			echo -n "|❗"  # Remote and local have diverged
		end
	end

	if git rev-parse --git-dir > /dev/null 2>&1  # We're in a git repo
		set_color blue
		echo -n -s "git(" (_git_branch_name)
		
		
		if not _is_git_clean
			echo -n "|⚠️ "
		end

		if not git status -sb | grep "## No commits yet" > /dev/null 2>&1  # There is at least one commit (to avoid weird errors)
			if [ (git rev-parse --abbrev-ref HEAD) != "HEAD" ]  # We're not in detached HEAD state
				if git branch -a | grep "remotes/origin/(_git_branch_name)"
					_print_git_branch_status
				end
			end
		end

		echo -n ") "
		set_color normal
	end
	echo -n '$ '
end
