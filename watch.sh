#!/bin/sh
# Adapted from watch by Eric Radman, 2015
# http://eradman.com/entrproject/scripts/

COMMAND="$@"
[ -z "$COMMAND" ] && COMMAND="./build.sh"

export IS_DEBUG_BUILD=true
export IS_WATCH_BUILD=true

# Sleep to allow Ctrl-C to operate
while sleep .5; do
	find \
		components \
		pages \
		styles \
		js \
		images \
		resources \
		auxiliary \
		utils \
		debug \
		favicon \
	-type f -print 2> /dev/null \
	| echo "$(cat)\nbuild.sh" \
	| entr -d $COMMAND
done
