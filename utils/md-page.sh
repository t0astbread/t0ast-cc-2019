#!/bin/bash
components/stylesheets.sh main
components/nav.sh

title=$(utils/page-metadata.sh $1 | yq r - title)
components/title.sh "$title" blog

echo "<main>"
    echo "<h1>$title</h1>"
    cat $1 | grep -v "#!" | pandoc - -s
echo "</main>"
