#!/bin/bash

# Extract the metadata portion (lines that start with "#! ", then lookup the "metadata" YAML property)
cat $1 | grep "#! " | sed -r 's/^#! //g' | yq r - metadata 2> /dev/null
