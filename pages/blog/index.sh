#!/bin/bash
components/stylesheets.sh main
components/title.sh blog
components/nav.sh

cat << EOF
<main>
    <h1>Blog</h1>
    All posts:
    <ul>
EOF

function post_li {
    page_path=$(utils/page-path.sh $1)
    echo -n "<li><a href=\"/$page_path\">"

    metadata_portion=$(utils/page-metadata.sh $1)
    
    title=$(echo "$metadata_portion" | yq r - title)
    if [ "$title" == "null" ]; then  # Use filename if metadata.title is undefined
        title=$(basename $page_path)
    fi

    created_time=$(echo "$metadata_portion" | yq r - created)
    if [ "$created_time" == "null" ]; then  # Use file's creation time if metadata.created is undefined
        created_time=$(date -d "@$(stat -c %W $1)" +%Y-%m-%d)
    fi

    components/two-liner.sh "$title" "$created_time"
    echo "</a></li>"
}
function post_ul {
    while read line; do
        post_li $line
    done
}
find pages/blog -type f | grep -v index.sh | post_ul

cat << EOF
    </ul>
</main>
EOF
