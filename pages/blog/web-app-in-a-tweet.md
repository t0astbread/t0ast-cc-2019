#!utils/md-page.sh

#! metadata:
#!  title: The "web app" that fits within a tweet
#!  created: 2019-02-04
#!  last_modified: 2019-08-16

This is a "web app" (it's actually just a tiny HTML fragment with a
little bit of JavaScript but whatever) that fits entirely within a
tweet (<=280 characters; in the final version).

It's a little script that flips the viewport's color between black
and white to illuminate the screen and use it as a weak light source.
It can be used either by clicking or via the keyboard.

The original authors of this "app" can be found
[in this Hacker News thread](https://news.ycombinator.com/item?id=19069487).
I merely modified it a bit to make it more user-friendly and
compressed.


## The code

```html
data:text/html,<title>Screen Light</title><html onclick=document.body.parentElement.onkeyup() onkeyup=o=!o;style.background=o%3F0:'black';style.color=o%3F0:'white' tabindex=0><body onload=o=1><h1>Screen Light</h1><p>Turn your screen into a light source<br>Tap to toggle<p><a href=https:/news.ycombinator.com/item?id=19069487>Source
```


## Installation

Just copy and paste the data URI into your browser's address bar.

You can also bookmark it to have it "downloaded". Note that data URIs
always work offline.
