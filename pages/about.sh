#!/bin/bash
components/stylesheets.sh main
components/title.sh about
components/nav.sh

echo "<main>"
echo "
# About

<section>
## About Me
Hello there!

My name's Michael though you will be able to find me as T0astBread
(or T0ast for short) on the internet.

I'm a student of IT and software engineering at an Austrian Higher
Technical College and I'm <strike>very</strike> (it varies)
interested in the subject.

You can check out what I'm working on on my <strike>GitHub</strike>
[various](https://gitlab.com/T0astBread)
[git remote host](https://github.com/T0astBread)
[places](https://git.sr.ht/~t0astbread) which I need to organize or,
for a more concise overview, the repos page on this site.
<strike>Also take a look at my finished projects.</strike> (Not
implemented yet in the rewrite of this website.)

</section>

<section>
## About this site
This site has been generated using a custom-built static site
generator based on CGI-like shell scripts. (Yes, I wrote my site in
bash.)

<figure>
![Language composition of this website, showing that 62.4% of its code is shell scripts](/images/sh-linguist.png)
<figcaption>I'm gonna hate this in six months</figcaption>
</figure>

I might write about it more in detail later.

</section>
" | pandoc -
echo "</main>"
