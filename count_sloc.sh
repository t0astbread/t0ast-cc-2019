#!/bin/sh
find . \( -path ./build -o -path ./.git \) -prune -o -type f -not -name "*.otf" -print | xargs cat | grep -cvE ^\s*$
