#!/bin/bash
echo "Building..."

find pages components utils \( -name "*.sh" -o -name "*.md" \) | xargs chmod +x  # Make everything executable

# Clean build dir
rm -r build
mkdir build

cp favicon/* build

# Build stylesheets
if [ $IS_DEBUG_BUILD ]; then
    sass styles/stylesheets:build/stylesheets
else
    sass --no-source-map --no-error-css --style=compressed styles/stylesheets:build/stylesheets
fi

# Build JS
if [ -d js ]; then
    cp -r js build/js
else
    mkdir build/js
fi

# Copy any fonts
if [ -d fonts ]; then
    cp -r fonts build/fonts
fi

# Copy any images
if [ -d images ]; then
    cp -r images build/images
fi

# Copy any resources
if [ -d resources ]; then
    find resources -type f -print | while read res_file; do
        res_path=$(echo "$res_file" | sed -E 's/^resources\///')
        mkdir -p "build/$(dirname $res_path)"
        cp $res_file build/$res_path
    done
fi

function render_page {
    page_file=$1
    output_file=$2

    is_html=true

    if [ -z $output_file ]; then
        page_path=$(utils/page-path.sh $page_file)
        output_file=build/$page_path

        # Does the page file name contain a dot? If so, it's probably not meant to be HTML
        [[ $(basename $page_file) == *.*.sh ]] && is_html=false
        
        if $is_html; then
            output_file=$output_file.html
        fi
    fi

    output_dir=$(dirname $output_file)
    mkdir -p $output_dir

    if $is_html; then
        components/html-boilerplate.sh >> $output_file

        # Load the autoreload script if we're building a watch build
        if [ $IS_WATCH_BUILD ]; then
            echo "<script src=\"/js/autoreload.js\"></script>" >> $output_file
        fi
    fi

    eval $page_file >> $output_file  # Just execute the page I guess
}
function render_pages {
    while read line; do
        render_page $line
    done
}
find pages \( -name "*.sh" -o -name "*.md" \) -print | render_pages

cp .htaccess build/

if [ $IS_DEBUG_BUILD ]; then
    cp debug/test2.css build/stylesheets

    render_page debug/test.sh build/test.html
    render_page debug/test2.sh build/test2.html
fi

if [ $IS_WATCH_BUILD ]; then
    cat /dev/urandom | base64 | head -c 5 > build/primer  # Generate the primer
    cp debug/autoreload.js build/js  # Include the autoreload script
fi
